package com.n.tatavason;

import android.content.Intent;
import android.os.Handler;

public class SplashActivity extends BaseActivity {


    @Override
    public void onCreateFinished() {
        setContentView(R.layout.activity_splash);

    }

    @Override
    public void afterInjection() {

        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }, 1000);

    }
}
