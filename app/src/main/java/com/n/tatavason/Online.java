package com.n.tatavason;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Online implements Parcelable {

    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("users")
    @Expose
    private Integer users;

    protected Online(Parcel in) {
        room = in.readString();
        if (in.readByte() == 0) {
            users = null;
        } else {
            users = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(room);
        if (users == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(users);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Online> CREATOR = new Creator<Online>() {
        @Override
        public Online createFromParcel(Parcel in) {
            return new Online(in);
        }

        @Override
        public Online[] newArray(int size) {
            return new Online[size];
        }
    };

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Integer getUsers() {
        return users;
    }

    public void setUsers(Integer users) {
        this.users = users;
    }

}