package com.n.tatavason;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferencesHelper {

    private static PreferencesHelper instance;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    private static final String ACCESS_TOKEN = "access_token";
    private static final String FIRST_OPEN = "firstOpen";
    private static final String CITY_MODEL = "city_model";
    private static final String LOGIN_USERNAME = "username";
    private static final String LOGIN_ENCRYP_PASSWD = "enc.password";
    private static final String IS_LOGOUT = "logout";
    private static final String AVATAR = "avatar";
    private static final String GIF = "gif";
    private static final String COMMON_ORG = "commonOrganization";
    private static final String SEANCE_LIST = "seanceList";
    private static final String TICKET_TYPE = "ticketTypeList";
    private static final String SELECTED_SALON = "selectedSalon";
    private static final String SELECTED_BRANCH = "selectedBranch";
    private static final String ORG_DETAIL = "orgDetail";
    private static final String SEAT_PLAN_URL = "seatPlanUrl";


    private PreferencesHelper(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPreferences.edit();
    }

    public static PreferencesHelper getInstance(Context context) {
        if (instance == null)
            instance = new PreferencesHelper(context);
        return instance;
    }


    public void setAccessToken(String token) {
        mEditor.putString(ACCESS_TOKEN, token);
        mEditor.commit();
    }

    public String getAccessToken() {
        return mPreferences.getString(ACCESS_TOKEN, "");
    }


    public void setAvatar(String avatar) {
        mEditor.putString(AVATAR, avatar);
        mEditor.commit();
    }

    public String getAvatar() {
        return mPreferences.getString(AVATAR, "");
    }


    public void setDisableGif(Boolean gif) {
        mEditor.putBoolean(GIF, gif);
        mEditor.commit();
    }

    public Boolean getDisableGif() {
        return mPreferences.getBoolean(GIF, false);
    }


    public void setLoginUsername(String username) {
        mEditor.putString(LOGIN_USERNAME, username);
        mEditor.commit();
    }

    public String getLoginUsername() {
        return mPreferences.getString(LOGIN_USERNAME, "");
    }


    public void setSeatPlanUrl(String seatPlanUrl) {
        mEditor.putString(SEAT_PLAN_URL, seatPlanUrl);
        mEditor.commit();
    }

    public String getSeatPlanUrl() {
        return mPreferences.getString(SEAT_PLAN_URL, "");
    }



    public void setFirstOpen(boolean firstOpen) {
        mEditor.putBoolean(FIRST_OPEN, firstOpen);
        mEditor.commit();
    }

    public boolean getFirstOpen() {
        return mPreferences.getBoolean(FIRST_OPEN, true);
    }

    public void setIsLogout(boolean logout) {
        mEditor.putBoolean(IS_LOGOUT, logout);
        mEditor.commit();
    }

    public boolean getIsLogout() {
        return mPreferences.getBoolean(IS_LOGOUT, false);
    }


    public void clear() {

        mEditor.clear().commit();
    }


}
