package com.n.tatavason;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitInterface {


    @GET("index.php?apikey=GCIO6-K2177-JCBIE-UNW7G-G8I1V&module=roomList&type=json")
    Call<JsonArray> getOnline();

    @GET("api/category/GetSosyal.php")
    Call<JsonArray> getSchoolList();


    @FormUrlEncoded
    @POST("category/PostReport.php")
    Call<JsonPrimitive> postComplaint(
            @FieldMap Map<String, String> body);
}
