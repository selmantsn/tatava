package com.n.tatavason;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.codingending.popuplayout.PopupLayout;

public class PopupDialog {

    public static PopupLayout popupMatch(Context context, DialogInfo dialogInfo) {
        View parent = View.inflate(context, R.layout.popup_match, null);

        final PopupLayout popupLayout = PopupLayout.init(context, parent);
        popupLayout.setUseRadius(true);

        Button btnClose = parent.findViewById(R.id.btnClose);
        Button btnMatch = parent.findViewById(R.id.btnMatch);

        TextView tvTitle = parent.findViewById(R.id.tvTitle);
        TextView tvContent = parent.findViewById(R.id.tvContent);

        //TextView tvErrorDesc = parent.findViewById(R.id.tvErrorDesc);
        //tvContent.setText(content);

        btnMatch.setOnClickListener(v -> {
            dialogInfo.okClick();
            popupLayout.dismiss();
        });

        btnClose.setOnClickListener(view -> {
            popupLayout.dismiss();
        });

        popupLayout.setDismissListener(() -> {
            dialogInfo.dismissListener();

        });

        popupLayout.show(PopupLayout.POSITION_CENTER);

        return popupLayout;
    }


    public abstract static class DialogInfo {

        public abstract void okClick();

        public abstract void dismissListener();

    }


}
