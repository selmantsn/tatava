package com.n.tatavason;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {


    @BindView(R.id.nav_view)
    NavigationView nav_view;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ripple1)
    ImageView ripple1;
    @BindView(R.id.ripple2)
    ImageView ripple2;
    @BindView(R.id.ripple3)
    ImageView ripple3;
    @BindView(R.id.ripple4)
    ImageView ripple4;
    @BindView(R.id.ripple5)
    ImageView ripple5;
    @BindView(R.id.ripple6)
    ImageView ripple6;
    @BindView(R.id.btnCall)
    RelativeLayout btnCall;
    @BindView(R.id.tvOnlineCount)
    TextView tvOnlineCount;

    private List<Online> onlines;


    @Override
    public void onCreateFinished() {
        setContentView(R.layout.activity_main);
    }

    @Override
    public void afterInjection() {

        setSupportActionBar(toolbar);

        getOnline();
        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                //call function
                getOnline();

                ha.postDelayed(this, 10000);
            }
        }, 10000);


        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        nav_view.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.anasayfa:
                    break;
                case R.id.universite:
                    startActivity(new Intent(this, SchoolListActivity.class));
                    break;
                case R.id.lise:
                    //startActivity(new Intent(this, SchoolListActivity.class));
                    break;
                case R.id.sikayet:
                    startActivity(new Intent(this, ComplaintActivity.class));
                    //startActivity(new Intent(this, SchoolListActivity.class));
                    break;
            }

            drawer.closeDrawer(GravityCompat.START);

            return true;
        });

        View headerview = nav_view.getHeaderView(0);
        LinearLayout headerLogo = headerview.findViewById(R.id.headerLogo);


        headerLogo.setOnClickListener(view -> {
            drawer.closeDrawer(GravityCompat.START);

        });

        btnCall.setOnClickListener(view -> {

            showCustomPopup("08508887204", "9", "Eşleştiğin kişiyi değiştirmek için “0” tuşlayabilirsin.");
            /*
            PopupDialog.popupMatch(this, new PopupDialog.DialogInfo() {
                @Override
                public void okClick() {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "08508887204,9"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    Activity#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for Activity#requestPermissions for more details.
                            return;
                        }
                    }
                    startActivity(intent);
                }

                @Override
                public void dismissListener() {

                }
            });


             */

        });

    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();

        ripple1.clearAnimation();
        ripple2.clearAnimation();
        ripple3.clearAnimation();
        ripple4.clearAnimation();
        ripple5.clearAnimation();
        ripple6.clearAnimation();

    }

    @Override
    protected void onResume() {
        super.onResume();
        ripple1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.effect));
        new Handler().postDelayed(() -> {
            ripple2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.effect));
            new Handler().postDelayed(() -> {
                ripple3.startAnimation(AnimationUtils.loadAnimation(this, R.anim.effect));
                new Handler().postDelayed(() -> {
                    ripple4.startAnimation(AnimationUtils.loadAnimation(this, R.anim.effect));
                    new Handler().postDelayed(() -> {
                        ripple5.startAnimation(AnimationUtils.loadAnimation(this, R.anim.effect));
                        new Handler().postDelayed(() -> {
                            ripple6.startAnimation(AnimationUtils.loadAnimation(this, R.anim.effect));
                        }, 1000);
                    }, 1000);
                }, 1000);
            }, 1000);
        }, 1000);
    }


    private void getOnline() {
        RetrofitClient.changeApiBaseUrl("http://tatava.kobikom.com.tr/api/");


        RetrofitInterface retrofitInterface = RetrofitClient.getApiClient().create(RetrofitInterface.class);
        Call<JsonArray> call = retrofitInterface.getOnline();

        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (isFinishing())
                    return;
                if (response.code() == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().toString());
                        onlines = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Online>>() {
                        }.getType());

                        if (onlines.get(0).getUsers() > 0) {
                            tvOnlineCount.setText(onlines.get(0).getUsers() + " kişi çevrimiçi");
                            tvOnlineCount.setTextColor(getResources().getColor(R.color.green));
                        } else {
                            tvOnlineCount.setText("Çevrimiçi Kullanıcı Yok");
                            tvOnlineCount.setTextColor(getResources().getColor(R.color.custom_white));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
            }
        });

    }


}
