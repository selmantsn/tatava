package com.n.tatavason;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonPrimitive;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintActivity extends BaseActivity {

    private static final String TAG = "ComplaintAct";
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.editReason)
    EditText editReason;
    @BindView(R.id.editSubject)
    EditText editSubject;
    @BindView(R.id.editEmail)
    EditText editEmail;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.btnSelectDate)
    LinearLayout btnSelectDate;
    @BindView(R.id.tvDate)
    TextView tvDate;

    protected SimpleDateFormat fullFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("tr"));

    @Override
    public void onCreateFinished() {
        setContentView(R.layout.activity_complaint);

    }

    @Override
    public void afterInjection() {

        back.setOnClickListener(view -> finish());

        btnSelectDate.setOnClickListener(view -> {
            showDateTimePicker();
        });

        btnSend.setOnClickListener(view -> {
            if (TextUtils.isEmpty(editEmail.getText().toString())) {
                Toast.makeText(this, "Email alanını doldurunuz!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(editSubject.getText().toString())) {
                Toast.makeText(this, "Şikayet konusu alanını doldurunuz!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(tvDate.getText().toString())) {
                Toast.makeText(this, "Görüşme zamanını seçiniz!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(editReason.getText().toString())) {
                Toast.makeText(this, "Şikayet nedeni alanını doldurunuz!", Toast.LENGTH_SHORT).show();
                return;
            }

            postComplaint();
        });

    }

    Calendar date;

    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            date.set(year, monthOfYear, dayOfMonth);
            new TimePickerDialog(this, (view1, hourOfDay, minute) -> {
                date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                date.set(Calendar.MINUTE, minute);

                tvDate.setText(fullFormat.format(date.getTime()));
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }


    private void postComplaint() {
        delayedProgressDialog.show();

        RetrofitClient.changeApiBaseUrl("http://sosyaluniversite.com/api/api/");


        Map<String, String> body = new HashMap<>();
        body.put("email", editEmail.getText().toString());
        body.put("report", editReason.getText().toString());
        body.put("report_reason", editSubject.getText().toString());
        body.put("calling_time", "");
        body.put("method", "insert");
        RetrofitInterface retrofitInterface = RetrofitClient.getApiClient().create(RetrofitInterface.class);


        Call<JsonPrimitive> call = retrofitInterface.postComplaint(body);

        call.enqueue(new Callback<JsonPrimitive>() {
            @Override
            public void onResponse(Call<JsonPrimitive> call, Response<JsonPrimitive> response) {
                delayedProgressDialog.cancel();
                if (isFinishing())
                    return;
                if (response.code() == 200) {
                    if (response.body().toString().equals("1")) {
                        Toast.makeText(ComplaintActivity.this, "Şikayetiniz başarıyla iletilmiştir.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(ComplaintActivity.this, "Şikayetiniz iletilemedi lütfen tekrar deneyin!", Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<JsonPrimitive> call, Throwable t) {
                delayedProgressDialog.cancel();


            }
        });

    }

}
