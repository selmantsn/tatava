package com.n.tatavason;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolListActivity extends BaseActivity {

    private static final String TAG = "SchoolListAct";
    @BindView(R.id.recSchool)
    RecyclerView recSchool;
    @BindView(R.id.btnSearch)
    ImageView btnSearch;
    @BindView(R.id.editSearch)
    EditText editSearch;
    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.pullToRefresh)
    SwipeRefreshLayout pullToRefresh;

    private SchoolAdapter adapter;
    private List<Online> onlines;

    private List<School> schoolList;


    @Override
    public void onCreateFinished() {
        setContentView(R.layout.activity_school_list);

    }

    @Override
    public void afterInjection() {
        getSchoolList();

        back.setOnClickListener(view -> finish());

        btnSearch.setOnClickListener(view -> {
            editSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editSearch, InputMethodManager.SHOW_IMPLICIT);
        });

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                List<School> searchedList = new ArrayList<>();

                for (int j = 0; j < schoolList.size(); j++) {
                    if (schoolList.get(j).getName().toLowerCase().contains(editSearch.getText().toString().toLowerCase())) {
                        searchedList.add(schoolList.get(j));
                    }
                }

                adapter = new SchoolAdapter(SchoolListActivity.this, searchedList);
                recSchool.setLayoutManager(new LinearLayoutManager(SchoolListActivity.this, RecyclerView.VERTICAL, false));
                recSchool.setAdapter(adapter);


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pullToRefresh.setOnRefreshListener(() -> {
            editSearch.getText().clear();
            getSchoolList();
            pullToRefresh.setRefreshing(false);
        });

    }

    private void getSchoolList() {
        delayedProgressDialog.show();

        RetrofitClient.changeApiBaseUrl("http://sosyaluniversite.com/api/");


        RetrofitInterface retrofitInterface = RetrofitClient.getApiClient().create(RetrofitInterface.class);
        Call<JsonArray> call = retrofitInterface.getSchoolList();
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                delayedProgressDialog.cancel();

                if (isFinishing())
                    return;
                if (response.code() == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().toString());
                        schoolList = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<School>>() {
                        }.getType());

                        getOnline(schoolList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                delayedProgressDialog.cancel();


            }
        });
    }


    private void getOnline(List<School> schools) {
        delayedProgressDialog.show();


        RetrofitClient.changeApiBaseUrl("http://tatava.kobikom.com.tr/api/");


        RetrofitInterface retrofitInterface = RetrofitClient.getApiClient().create(RetrofitInterface.class);
        Call<JsonArray> call = retrofitInterface.getOnline();

        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                delayedProgressDialog.cancel();


                if (isFinishing())
                    return;
                if (response.code() == 200) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().toString());
                        onlines = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Online>>() {
                        }.getType());
                        Log.i(TAG, "onResponse: online");

                        for (int i = 0; i < schools.size(); i++) {
                            schools.get(i).setOnline(onlines.get(i).getUsers());
                        }

                        adapter = new SchoolAdapter(SchoolListActivity.this, schools);
                        recSchool.setLayoutManager(new LinearLayoutManager(SchoolListActivity.this, RecyclerView.VERTICAL, false));
                        recSchool.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                delayedProgressDialog.cancel();


            }
        });

    }


}
