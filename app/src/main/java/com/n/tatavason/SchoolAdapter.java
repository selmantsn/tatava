package com.n.tatavason;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.MyViewHolder> {

    private Context context;
    private List<School> schoolLists;

    public SchoolAdapter(Context context, List<School> schoolLists) {
        this.context = context;
        this.schoolLists = schoolLists;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_school, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        School schoolList = schoolLists.get(position);

        Glide.with(context).load(schoolList.getImage()).apply((new RequestOptions().placeholder(android.R.color.white))).into(holder.icLogo);

        holder.tvSchoolName.setText(schoolList.getName());
        holder.tvSchoolNumber.setText(schoolList.getPhone());
        holder.tvOnlineNumber.setText(schoolList.getOnline() + " kişi çevrimiçi");

        holder.icCall.setOnClickListener(view -> {
            ((BaseActivity) context).showCustomPopup(schoolList.getPhone(), schoolList.getRoom(), schoolList.getName());
        });

    }


    @Override
    public int getItemCount() {
        if (schoolLists == null) {
            return 0;
        }
        return schoolLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.icLogo)
        ImageView icLogo;
        @BindView(R.id.icCall)
        ImageView icCall;
        @BindView(R.id.tvSchoolName)
        TextView tvSchoolName;
        @BindView(R.id.tvSchoolNumber)
        TextView tvSchoolNumber;
        @BindView(R.id.tvOnlineNumber)
        TextView tvOnlineNumber;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
