package com.n.tatavason;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public abstract class BaseActivity extends AppCompatActivity {


    public static final int CALL_PERMISSON = 1;


    private static final String TAG = "BaseTAG";
    private String key;

    private Unbinder unbinder;

    protected DelayedProgressDialog delayedProgressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/HelveticaNeue-Medium-11.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        onCreateFinished();

        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.addFragmentManager(getSupportFragmentManager());


        unbinder = ButterKnife.bind(this);

/*
        delayedProgressDialog = KProgressHUD.create(this)
                .setSize(100, 100)
                .setCancellable(false);


 */

        afterInjection();

    }


    protected PreferencesHelper getPreferencesHelper() {
        return PreferencesHelper.getInstance(this);
    }

    public abstract void onCreateFinished();

    public abstract void afterInjection();


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ((delayedProgressDialog != null) && delayedProgressDialog.isVisible())
            delayedProgressDialog.dismiss();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CALL_PERMISSON:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                  /*
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + uniNumber));
                    startActivity(intent);

                   */
                }
        }

    }

    public void showCustomPopup(String uniNumber, String roomNumber, String uniName) {

        Dialog mydialog = new Dialog(this);

        mydialog.setContentView(R.layout.popup_match);

        Button btnClose = mydialog.findViewById(R.id.btnClose);
        Button btnMatch = mydialog.findViewById(R.id.btnMatch);
        TextView tvTitle = mydialog.findViewById(R.id.tvTitle);

        tvTitle.setText(uniName);

        btnClose.setOnClickListener(view1 -> {
            mydialog.dismiss();
        });

        btnMatch.setOnClickListener(view12 -> {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + uniNumber + "," + roomNumber + Uri.encode("#")));
                startActivity(intent);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSON);
            }
            /*
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this, R.style.SheetDialog);
            bottomSheetDialog.setContentView(R.layout.popup_call);

            LinearLayout btnCallNumber = bottomSheetDialog.findViewById(R.id.btnCallNumber);
            RelativeLayout btnCancel = bottomSheetDialog.findViewById(R.id.btnCancel);
            TextView tvNumber = bottomSheetDialog.findViewById(R.id.tvNumber);

            tvNumber.setText("Ara " + uniNumber);

            btnCallNumber.setOnClickListener(view -> {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + uniNumber));
                    startActivity(intent);
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSON);
                }
            });

            btnCancel.setOnClickListener(view -> {
                bottomSheetDialog.dismiss();
            });

            bottomSheetDialog.show();

            mydialog.dismiss();

             */
        });

        mydialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mydialog.show();
    }
}
